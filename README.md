# React Native Reddit Client

## Motivation

This project is just made to show some of the features of React Native

## Scope

This is not a final product, just an MVP for demo purposes.

## Features

🗸 - Use a `FlatList` to display the last posts of [/r/pics](https://www.reddit.com/r/pics/) subreddit.

🗸 - Show basic information of each post

🗸 - Show the picture on a new `WebView`

🗸 - Refreshable! 🥗

## Even More features

🗸 - Typescript

🗸 - Redux

🗸 - Unit tests with Jest

🗸 - Select the feed ordering

### Enjoy!