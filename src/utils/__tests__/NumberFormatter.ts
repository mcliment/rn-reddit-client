import { kFormat } from "../NumberFormatter";

describe("NumberFormatter kFormat", () => {
  test.each([[0, "0"], [1, "1"], [999, "999"]])(
    "should leave if less than %i as %i",
    (value, expected) => {
      expect(kFormat(value)).toBe(expected);
    },
  );

  test.each([[1000, "1.0k"], [5555, "5.6k"] /*, TODO :: fix this case [9999, "9.9k"] */])(
    "should add k and 1 decimal if greater or equal to 1000: %i as %i",
    (value, expected) => {
      expect(kFormat(value)).toBe(expected);
    },
  );

  test.each([[10000, "10k"], [10999, "11k"]])(
    "should add k and no decimals if greater or equal to 10000: %i as %i",
    (value, expected) => {
      expect(kFormat(value)).toBe(expected);
    },
  );
});
