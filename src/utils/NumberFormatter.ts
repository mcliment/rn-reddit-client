
/**
 * Shortens numbers greater than 1000 adding a k at the end
 *
 * @export
 * @param {number} value - The number to format
 * @returns A string representing the formatted number
 */
export function kFormat(value: number) {
  if (value >= 10000) { return (value / 1000).toFixed(0) + "k"; }
  if (value >= 1000) { return (value / 1000).toFixed(1) + "k"; }
  return value.toString();
}
