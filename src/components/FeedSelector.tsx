import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

import { FeedOptions } from "../services/RedditApi";

import FeedSelectorButton from "./FeedSelectorButton";

interface IProps {
  selectedFeed: FeedOptions;
  onSelectFeed(feed: FeedOptions): any;
}

export default class FeedSelector extends Component<IProps> {
  public render() {
    const { selectedFeed, onSelectFeed } = this.props;

    return (
      <View style={styles.feedSelector}>
        <FeedSelectorButton
          label="New"
          selected={selectedFeed === "new"}
          onPress={() => onSelectFeed("new")}
        />
        <FeedSelectorButton
          label="Hot"
          selected={selectedFeed === "hot"}
          onPress={() => onSelectFeed("hot")}
        />
        <FeedSelectorButton
          label="Contr."
          selected={selectedFeed === "controversial"}
          onPress={() => onSelectFeed("controversial")}
        />
        <FeedSelectorButton
          label="Top"
          selected={selectedFeed === "top"}
          onPress={() => onSelectFeed("top")}
        />
        <FeedSelectorButton
          label="Rising"
          selected={selectedFeed === "rising"}
          onPress={() => onSelectFeed("rising")}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  feedSelector: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
