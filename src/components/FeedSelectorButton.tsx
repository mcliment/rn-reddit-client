import React, { Component } from "react";
import {
  GestureResponderEvent,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
} from "react-native";

interface IProps {
  label: string;
  selected: boolean;
  onPress?: (event: GestureResponderEvent) => void;
}

export default class FeedSelectorButton extends Component<IProps> {
  public render() {
    const { label, selected, onPress } = this.props;

    return (
      <TouchableNativeFeedback onPress={onPress}>
        <Text
          style={
            selected
              ? styles.feedSelectorLabelSelected
              : styles.feedSelectorLabel
          }
        >
          {label}
        </Text>
      </TouchableNativeFeedback>
    );
  }
}

const styles = StyleSheet.create({
  feedSelectorLabel: {
    flex: 1,
    paddingBottom: 10,
    paddingTop: 10,
    textAlign: "center",
  },
  feedSelectorLabelSelected: {
    backgroundColor: "#a0a0a0",
    flex: 1,
    fontWeight: "bold",
    paddingBottom: 10,
    paddingTop: 10,
    textAlign: "center",
  },
});
