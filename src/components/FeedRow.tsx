import React, { Component } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  View,
} from "react-native";
import { NavigationScreenProp } from "react-navigation";

import { kFormat } from "../utils/NumberFormatter";

import moment from "moment";

interface IProps {
  text: string;
  thumbnail: string;
  created: number;
  author: string;
  score: number;
  comments: number;
  link: string;
  navigation: NavigationScreenProp<any, any>;
}

export default class FeedRow extends Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  public render() {
    const { navigate } = this.props.navigation;
    const { text, link, thumbnail, created, score, comments } = this.props;
    const dt = moment(created, "X");

    const [formattedScore, formattedComments] = [
      kFormat(score),
      kFormat(comments),
    ];

    return (
      <TouchableNativeFeedback
        onPress={() => navigate("Post", { url: link, title: text })}
      >
        <View style={styles.row}>
          <Image style={styles.image} source={{ uri: thumbnail }} />
          <View style={styles.rowContent}>
            <Text style={styles.date}>{dt.fromNow()}</Text>
            <Text>
              <Text style={styles.title}>{text}</Text>
            </Text>
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <View style={{ flex: 2 }}>
                <Text style={styles.author}>
                  u/
                  {this.props.author}
                </Text>
              </View>
              <View
                style={{
                  alignItems: "center",
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                }}
              >
                <Image
                  style={{ height: 14, resizeMode: "contain" }}
                  source={require("../images/up.png")}
                />
                <Text style={styles.score}>{formattedScore}</Text>
              </View>
              <View
                style={{
                  alignItems: "center",
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                }}
              >
                <Image
                  style={{ height: 14, resizeMode: "contain" }}
                  source={require("../images/comments.png")}
                />
                <Text style={styles.comments}>{formattedComments}</Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableNativeFeedback>
    );
  }
}

const styles = StyleSheet.create({
  author: {
    color: "#787c7e",
    flex: 2,
    fontSize: 14,
  },
  comments: {
    color: "#878a8c",
    flex: 1,
    fontSize: 14,
    fontWeight: "bold",
  },
  date: {
    color: "#787c7e",
    fontSize: 12,
    textAlign: "right",
  },
  image: {
    height: 100,
    width: 100,
  },
  row: {
    alignItems: "center",
    flex: 1,
    flexDirection: "row",
    padding: 6,
  },
  rowContent: {
    flex: 1,
    flexDirection: "column",
    paddingLeft: 8,
  },
  score: {
    color: "#1a1a1b",
    flex: 1,
    fontSize: 14,
    fontWeight: "bold",
  },
  title: {
    color: "#222222",
    fontSize: 20,
  },
});
