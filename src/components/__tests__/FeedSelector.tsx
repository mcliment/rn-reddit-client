import "react-native";

import React from "react";
import renderer from "react-test-renderer";

import FeedSelector from "../FeedSelector";

it("renders without crashing", () => {
  const voidFunction = () => null;

  const rendered = renderer.create(<FeedSelector selectedFeed="new" onSelectFeed={voidFunction} />).toJSON();
  expect(rendered).toMatchSnapshot();
});
