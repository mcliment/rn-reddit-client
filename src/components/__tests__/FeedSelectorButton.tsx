import "react-native";

import React from "react";
import renderer from "react-test-renderer";

import FeedSelectorButton from "../FeedSelectorButton";

it("renders without crashing", () => {
  const rendered = renderer.create(<FeedSelectorButton label="Test" selected={true} />).toJSON();
  expect(rendered).toMatchSnapshot();
});
