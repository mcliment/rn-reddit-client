import "react-native";

import React from "react";
import renderer from "react-test-renderer";

import HomeLogo from "../HomeLogo";

it("renders without crashing", () => {
  const rendered = renderer.create(<HomeLogo />).toJSON();
  expect(rendered).toMatchSnapshot();
});
