import "react-native";

import React from "react";
import renderer from "react-test-renderer";

import FeedRow from "../FeedRow";

it("renders without crashing", () => {
  const rendered = renderer
    .create(
      <FeedRow
        text={""}
        thumbnail={""}
        created={123}
        author={""}
        score={0}
        comments={0}
        link={""}
        navigation={{} as any}
      />,
    )
    .toJSON();
  expect(rendered).toMatchSnapshot();
});
