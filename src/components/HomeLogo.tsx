import React, { Component } from "react";
import { Image, StyleSheet, View } from "react-native";

export default class HomeLogo extends Component {
  public render() {
    return (
      <View>
        <Image
          style={styles.logo}
          source={require("../images/reddit_pics.png")}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    alignSelf: "center",
  },
});
