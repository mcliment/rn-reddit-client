import { AnyAction } from "redux";
import { FeedOptions } from "../services/RedditApi";

import { REQUEST_FEED, REQUEST_REFRESH, UPDATE_FEED } from "../actions";

export interface IState {
  items: any[];
  loading: boolean;
  refreshing: boolean;
  selectedFeed: FeedOptions;
}

const initialState: IState = {
  items: [],
  loading: true,
  refreshing: false,
  selectedFeed: "new",
};

export function reddit(state = initialState, action: AnyAction): IState {
  switch (action.type) {
    case REQUEST_REFRESH:
      return { ...state, items: [] };
    case REQUEST_FEED:
      return { ...state, items: [], selectedFeed: action.feed, loading: true };
    case UPDATE_FEED:
      return { ...state, items: action.items, loading: false, refreshing: false };
    default:
      return state;
  }
}
