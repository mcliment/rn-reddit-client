import fetch from "jest-fetch-mock";
import RedditApi from "../RedditApi";

describe("API is called", () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  test("is called with the proper feed", async () => {
    fetch.mockResponseOnce(JSON.stringify({ data: "test" }));

    const json = await RedditApi.fetchPosts("hot");

    expect(json.data).toBe("test");

    // Check arguments
    expect(fetch.mock.calls.length).toEqual(1);
    expect(fetch.mock.calls[0][0]).toMatch("hot.json");
  });

  test("fails with bad response", async () => {
    fetch.mockReject(new Error("fake error"));

    try {
      await RedditApi.fetchPosts("hot");
    } catch (error) {
      expect(error.message).toMatch("fake error");
    }
  });
});
