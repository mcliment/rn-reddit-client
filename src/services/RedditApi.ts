export type FeedOptions = "hot" | "new" | "controversial" | "top" | "rising";

export default class RedditApi {
  public static async fetchPosts(feed: FeedOptions) {
    try {
      const posts = await fetch(`https://api.reddit.com/r/pics/${feed}.json`);
      if (!posts.ok) {
        throw new Error(`Unexpected HTTP response fetching Reddit feed: ${posts.status} ${posts.statusText}`);
      }
      return posts.json();
    } catch (error) {
      throw error;
    }
  }
}
