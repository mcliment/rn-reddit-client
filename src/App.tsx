/**
 * Reddit Feed Reader
 */
import React from "react";

import { createStackNavigator } from "react-navigation";

import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";

import { reddit } from "./reducers";

import Home from "./screens/Home";
import Post from "./screens/Post";

const App = createStackNavigator({
  Home: { screen: Home },
  Post: { screen: Post },
});

const store = createStore(reddit, applyMiddleware(thunk));

export default () => (
  <Provider store={store}>
    <App />
  </Provider>);
