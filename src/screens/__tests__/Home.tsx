import "react-native";

import React from "react";
import renderer from "react-test-renderer";

import { Provider } from "react-redux";
import configureStore from "redux-mock-store";

import Home from "../Home";

const mockStore = configureStore();

const nullAction = () => null;

it("renders without crashing", () => {
  const store = mockStore({});

  const rendered = renderer
    .create(
      <Provider store={store}>
        <Home
          navigation={{} as any}
          items={[]}
          loading={false}
          refreshing={false}
          selectedFeed={"new"}
          onRefreshFeed={nullAction}
          onSelectFeed={nullAction}
        />
      </Provider>,
    )
    .toJSON();
  expect(rendered).toMatchSnapshot();
});
