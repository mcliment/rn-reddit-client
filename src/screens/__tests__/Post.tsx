import "react-native";

import React from "react";
import renderer from "react-test-renderer";

import Post from "../Post";

import { NavigationScreenProp } from "react-navigation";

const NavigatorMock = jest.fn<NavigationScreenProp<any, any>>(() => ({
  getParam: jest.fn(),
}));

it("renders without crashing", () => {
  const rendered = renderer
    .create(<Post navigation={new NavigatorMock()} />)
    .toJSON();
  expect(rendered).toMatchSnapshot();
});
