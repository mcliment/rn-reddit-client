import React from "react";
import { Component } from "react";
import { WebView } from "react-native";
import { NavigationScreenProp } from "react-navigation";

interface IProps {
  navigation: NavigationScreenProp<any, any>;
}

export default class Post extends Component<IProps> {
  public static navigationOptions = ({ navigation }: any) => {
    return {
      title: navigation.getParam("title", "Reddit post"),
    };
  }

  public render() {
    const { navigation } = this.props;

    const uri = navigation.getParam("url");

    return <WebView source={{ uri }} />;
  }
}
