import React from "react";
import { Component } from "react";
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  View,
} from "react-native";
import { NavigationScreenProp } from "react-navigation";

import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";

import FeedRow from "../components/FeedRow";
import FeedSelector from "../components/FeedSelector";
import HomeLogo from "../components/HomeLogo";

import { FeedOptions } from "../services/RedditApi";

import { refreshFeed, selectFeed } from "../actions";
import { IState } from "../reducers";

interface IProps {
  navigation: NavigationScreenProp<any, any>;
  items: any[];
  loading: boolean;
  refreshing: boolean;
  selectedFeed: FeedOptions;
}

interface IActions {
  onRefreshFeed(): any;
  onSelectFeed(feed: FeedOptions): any;
}

const mapStateToProps = (state: IState, props: IProps & IActions): IProps => {
  return {
    items: state.items,
    loading: state.loading,
    navigation: props.navigation,
    refreshing: state.refreshing,
    selectedFeed: state.selectedFeed,
  };
};

const mapDispatchToProps = (
  dispatch: ThunkDispatch<any, any, any>,
): IActions => {
  return {
    onRefreshFeed: () => {
      dispatch(refreshFeed());
    },
    onSelectFeed: (feed: FeedOptions) => {
      dispatch(selectFeed(feed));
    },
  };
};

class Home extends Component<IProps & IActions> {
  public static navigationOptions = {
    headerTitle: <HomeLogo />,
  };

  public async componentDidMount() {
    this.props.onSelectFeed("new");
  }

  public render() {
    const {
      loading,
      refreshing,
      items,
      selectedFeed,
      onRefreshFeed,
      onSelectFeed,
    } = this.props;
    const { navigation } = this.props;

    return (
      <View style={styles.container}>
        {loading && (
          <ActivityIndicator
            size="large"
            color="#ff4500"
            animating={loading}
            style={{ paddingTop: 30 }}
          />
        )}

        <FlatList
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefreshFeed} />
          }
          data={items}
          keyExtractor={this._keyExtractor}
          renderItem={({ item }) => (
            <FeedRow
              navigation={navigation}
              text={item.title}
              thumbnail={item.thumbnail}
              created={item.created_utc}
              author={item.author}
              score={item.score}
              comments={item.num_comments}
              link={item.url}
            />
          )}
        />

        <FeedSelector selectedFeed={selectedFeed} onSelectFeed={onSelectFeed} />
      </View>
    );
  }

  // tslint:disable-next-line:variable-name
  private _keyExtractor = (item: any, index: number) => {
    return item.id;
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFFFFF",
    flex: 1,
    justifyContent: "center",
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
