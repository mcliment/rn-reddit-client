import configureStore from "redux-mock-store";
import thunk from "redux-thunk";

import fetch from "jest-fetch-mock";

const mockStore = configureStore([thunk]);

import {
  refreshFeed,
  REQUEST_FEED,
  REQUEST_REFRESH,
  selectFeed,
  UPDATE_FEED,
} from "..";

describe("Test actions", () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  test("should fetch new feed", async () => {
    fetch.mockResponseOnce(
      JSON.stringify({ data: { children: [{ data: { test: "test" } }] } }),
    );

    const store = mockStore({});

    await store.dispatch(selectFeed("hot") as any);

    const actions = store.getActions();

    expect(actions.length).toBe(2);

    const request = actions[0];
    expect(request.type).toBe(REQUEST_FEED);
    expect(request.feed).toBe("hot");

    const response = actions[1];
    expect(response.type).toBe(UPDATE_FEED);
    expect(response.feed).toBe("hot");
    expect(response.items).toEqual([{ test: "test" }]);
  });

  test("should refresh current", async () => {
    fetch.mockResponseOnce(
      JSON.stringify({ data: { children: [{ data: { test: "test" } }] } }),
    );

    const store = mockStore({ selectedFeed: "rising" });

    await store.dispatch(refreshFeed() as any);

    const actions = store.getActions();

    expect(actions.length).toBe(2);

    const request = actions[0];
    expect(request.type).toBe(REQUEST_REFRESH);

    const response = actions[1];
    expect(response.type).toBe(UPDATE_FEED);
    expect(response.feed).toBe("rising");
    expect(response.items).toEqual([{ test: "test" }]);
  });
});
