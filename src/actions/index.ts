// tslint:disable:object-literal-sort-keys

import { AnyAction, Dispatch } from "redux";
import RedditApi, { FeedOptions } from "../services/RedditApi";

import { IState } from "../reducers";

export const REQUEST_FEED = "REQUEST_FEED";
const requestFeed = (feed: FeedOptions): AnyAction => ({
  type: REQUEST_FEED,
  feed,
});

export const UPDATE_FEED = "UPDATE_FEED";
const updateFeed = (feed: FeedOptions, items: any[]): AnyAction => ({
  type: UPDATE_FEED,
  feed,
  items,
});

export const REQUEST_REFRESH = "REQUEST_REFRESH";
const requestRefresh = (): AnyAction => ({
  type: REQUEST_REFRESH,
});

export function selectFeed(feed: FeedOptions) {
  return async (dispatch: Dispatch) => {
    dispatch(requestFeed(feed));

    const posts = await RedditApi.fetchPosts(feed);
    const items = posts.data.children.map((c: any) => c.data);

    dispatch(updateFeed(feed, items));
  };
}

export function refreshFeed() {
  return async (dispatch: Dispatch, getState: () => IState) => {
    dispatch(requestRefresh());

    const feed = getState().selectedFeed;
    const posts = await RedditApi.fetchPosts(feed);
    const items = posts.data.children.map((c: any) => c.data);

    dispatch(updateFeed(feed, items));
  };
}
